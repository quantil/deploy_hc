################### -------------- Run: QuantRisk --------------  ################### 
require(shiny)
### --- Path inst/application --- ### 
folder_address = '/root/deploy/auth/Shiny/QuantRisk/inst/application'
runApp(appDir =folder_address, port = 80,
       launch.browser = getOption("shiny.launch.browser", interactive()),
       host = getOption("shiny.host", "0.0.0.0"), workerId = "",
       quiet = FALSE, display.mode = c("auto", "normal", "showcase"),
       test.mode = getOption("shiny.testmode", FALSE))
################### -------------------------------------------  ################### 