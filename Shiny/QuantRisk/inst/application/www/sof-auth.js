$(document).on("click", "#submit_sign_in", () => {
  const email = $("#email").val();
  const password = $("#password").val();
  const login_attempt = {
    'email': email,
    'password': password
  };
  Shiny.setInputValue('login_attempt', login_attempt, {priority: "event"});
});

Shiny.addCustomMessageHandler('response_login', function(message) {
  if(message != "ok"){
    console.log("Malas credenciales")
    $("#respuesta_login").text("El usuario y/o contraseña es(son) inválido(s)");
  }
  else if (message == "ok") {
    console.log("Credenciales exitosas")
    $("#respuesta_login").text("Credenciales validadas");
  }
  else {
    console.log("Mensaje de respuesta inválido")
    $("#respuesta_login").text("el mensaje de respuesta es inválido. Hablé con TI");
  }
});




