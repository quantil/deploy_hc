import ldap
# based on https://gist.github.com/ibeex/1288159
def test_check_credentials(username, password):
  """Verifies credentials for username and password.
  Returns None on success or a string describing the error on failure
  # Adapt to your needs
  """
  session = {
    "status": "ok",
    username: "myusername"
  }
  return session


def check_credentials(username, password):
  """Verifies credentials for username and password.
  Returns None on success or a string describing the error on failure
  # Adapt to your needs
  """
  LDAP_SERVER = 'ldap://10.23.15.61:389'
  # fully qualified AD user name
  sodimac_suffix = "@sodimac.corp"
  if isinstance(username, str) and not username.endswith(sodimac_suffix):
    username += sodimac_suffix
  LDAP_USERNAME = username
  # your password
  LDAP_PASSWORD = password
  #base_dn = 'DC=xxx,DC=xxx'
  session = None
  try:
      # build a client
      ldap_client = ldap.initialize(LDAP_SERVER)
      print("Se inicializa conexión con LDAP!")
      # perform a synchronous bind
      ldap_client.set_option(ldap.OPT_REFERRALS,0)
      ldap_client.simple_bind_s(LDAP_USERNAME, LDAP_PASSWORD)
      ldap_client.search_st('DC=sodimac,DC=corp', ldap.SCOPE_SUBTREE, '(&(sAMAccountName=={})(member=OU="Grupos de Seguridad",OU=Homecenter))'.format(LDAP_USERNAME))
      print("SUCCESS!")
  except ldap.OPERATIONS_ERROR:
      ldap_client.unbind()
      return {
        "status": "failed-operations",
        "error": "Wrong username or password",
        username: None
      }
  except ldap.INVALID_CREDENTIALS:
      ldap_client.unbind()
      return {
        "status": "failed",
        "error": "Wrong username or password",
        username: None
      }
  except ldap.SERVER_DOWN:
    return {
      "status": "failed",
      "error": "Active Directory server is not available",
      username: None
    }

  # all is well
  # get all user groups and store it in a session dict for future use
  session = {
    "status": "ok",
    "username": username
  }
  ldap_client.unbind()

  return session